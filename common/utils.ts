// this is common methods used throughout the site
// usually for hacky functions or things we needed to
// custom code around particular weirdness
import * as path from "path";
import {browser, by, element, ElementFinder, ExpectedConditions, Locator} from "protractor";
import * as webdriver from "selenium-webdriver";
import wdPromise = webdriver.promise.Promise;

export class Utils {

    // this function was needed to click the checkboxes in the policy purchase page
    // clicking on the checkbox labels normally would click a hyperlink and open up new
    // tabs, which was a pain. Instead, we click using webdriver, which allows us to use an offset
    public static clickCheckBox(css: string, offset = { x: 10, y: 1 }): wdPromise<void> {
        // que up a move move to element, with a click
        return browser.actions().mouseMove(element(by.css(css)), offset).click().perform();
    }

    public static clearAndType(id: string, txt: string): wdPromise<void[]> {
        return webdriver.promise.all([
            element(by.id(id)).clear(),
            element(by.id(id)).sendKeys(txt),
        ]);
    }

    public static clickByMethod(ngClickMethod: string): wdPromise<void> {
        return element(by.css("button[ng-click^='" + ngClickMethod + "(']")).click();
    }

    public static sendIndividualKeys(el: ElementFinder, txt: string): wdPromise<void[]> {
        const sendKeysPromises = [];
        for (let i: number = 0; i < txt.length; i++) {
            sendKeysPromises.push(el.sendKeys(txt.charAt(i)));
        }

        return webdriver.promise.all(sendKeysPromises);
    }

    public static switchToNextTab(): wdPromise<void> {
        // wait until there is more than one tab
        return browser.wait(() => {
            return browser.getAllWindowHandles().then((tabs) => {
                return tabs.length > 1;
            });
        }, 10000)
        // once there is more than one tab, get list of all tabs
        .then(() => {
            return browser.getAllWindowHandles();
        })
        // switch to the furthest tab on the right
        .then((tabs: string[]) => {
            return browser.switchTo().window(tabs[tabs.length - 1]);
        });
    }

    public static closeCurrentTab(): wdPromise<void> {
        // close current tab
        browser.close();

        // get all tabs and select the first one
        return browser.getAllWindowHandles().then((tabs) => {
            return browser.switchTo().window(tabs[0]);
        });
    }

    public static validateHyperlink(locator: Locator, expectedUrl: string): wdPromise<void> {
        return element.all(locator).first().click()
        .then(() => {
            return Utils.switchToNextTab();
        })
        .then(() => {
            expect(browser.driver.getCurrentUrl()).toContain(expectedUrl);
        }).then(() => {
            return Utils.closeCurrentTab();
        });
    }

    public static validatePdfTab(): wdPromise<void> {
        Utils.switchToNextTab();

        const findCountPromise = browser.driver.findElements(by.css("embed")).then((elems) => {
            return elems.length > 0;
        });
        expect(findCountPromise).toBeTruthy();

        return Utils.closeCurrentTab();
    }

    public static sendKeysByModel(model: string, txt: string): wdPromise<void> {
        const el = element(by.css("input[ng-model*='" + model + "']"));

        // in this case, I want to return a single promise, because it's being using in a
        // promise.all function later
        return el.clear().then(() => {
            return el.sendKeys(txt);
        });
    }

    public static getTextByModel(model: string): wdPromise<string> {
        return element(by.css("input[ng-model*='" + model + "']")).getAttribute("value");
    }

    // This function clicks the first visible element when there are many on the
    // page with the same css properties
    public static clickFirstVisible(css: string): wdPromise<void> {
        const el = element.all(by.css(css)).filter( (elm) => {
            return elm.isDisplayed().then((isDisplayed) => {
                return isDisplayed;
            });
        }).first();
        return el.click();
    }

    // this function uses a particualarly general selector.
    // if possible, use more specific selectors like by.id, by.model, or even by.css
     public static expectTextByCss(cssClass: string, textToFind: string): wdPromise<void> {
        // use the special cssContainingText method
        return element.all(by.cssContainingText(cssClass, textToFind)).count().then((ct: number) => {
            // return the expectation, with a more detailed error message
            expect(ct).toBeGreaterThan(0, textToFind + " was not found on page");
        });
    }

    // function for clicking an inline radio button.
    // pass the id of the radio input to this function.
    // (e.g. for the radio label <label for="fileClaimDetailsForm_formalClaim_1">Yes</label>
    //  pass fileClaimDetailsForm_formalClaim_1 to this function)
    public static clickInlineRadio(cssString: string) {
        const offset = {
            x: 13,
            y: 13,
        };

        // que up a move to element, with a click
        return browser.actions().mouseMove(element(by.css(cssString)), offset).click().perform();
    }

    // this inserts the value into the element directly using javascript
    // its being used on the ATI login page, which was have very inconsistent
    // behavior using normal protractr
    public static jsSendValue(id: string, txt: string): wdPromise<void> {
        return browser.executeScript("document.getElementById('" + id + "').value='" + txt + "'");
    }

    // this function enters text and waits for the ajax response of the autocomplete
    // before tabbing to the next field
    public static sendTextForAutoComplete(id: string, ddlId: string, txt: string): wdPromise<void[]> {
        return webdriver.promise.all([element(by.id(id)).sendKeys(txt),
            browser.wait(ExpectedConditions.elementToBeClickable(element(by.id(ddlId)))),
            element(by.id(id)).sendKeys("\t"),
        ]);
    }

    // This is a pretty complex function that I'm not 100% we should even use.
    // Essentially, there are angular process in ATI that cause protractor to wait
    // for excessively long periods of time before executing tests. To solve that,
    // we can disable the angular wait, but we then lose the protractor functionality
    // that tells us elements are ready to be selected. We must do it ourselves first.
    // This function takes two arguments:
    // 1) A callback function, which is the protractor code from the calling function
    // 2) An optional selector, which we use to determine if the page is ready to test. The
    //    default selector is the body element of the page.
    public static smartExecute(callback: () => void, locator: Locator = by.css("body")): wdPromise<boolean> {

        // disable the angular wait first
        browser.waitForAngularEnabled(false);

        // runs a promise that won't resolve until the element is on the page, visible, and clickable
        // times out at 30 seconds
        return browser.wait(
            ExpectedConditions.elementToBeClickable(element(locator)),
            30000,
        ).then(() => {
            // run the code provided in the callback function
            return callback();
        }).then(() => {
            // re-enable the angular wait
            return browser.waitForAngularEnabled(true);
        });
    }

    public static uploadDocument(packageName: string, fileName: string) {
        // const fileToUpload = "../../testData/Utils/" + packageName + "/" + fileName;
        // const absolutePath = path.resolve(__dirname, fileToUpload);
        // // const remote = require("selenium-webdriver/remote");
        // // browser.setFileDetector(new remote.FileDetector());
        // const fileElem = element.all(by.css('input[type="file"]')).last();
        // // Find the file input element
        // fileElem.sendKeys(absolutePath);
        const remote = require("selenium-webdriver/remote");
        browser.setFileDetector(new remote.FileDetector());

        // requires an absolute path
        const fileToUpload = "../../testData/common/" + packageName + "/" + fileName;
        const absolutePath = path.resolve(__dirname, fileToUpload);

        // Find the file input element
        const fileElem = element.all(by.css('input[type="file"]')).last();

        // Need to unhide flowjs's secret file uploader
        browser.executeScript("arguments[0].style.visibility = 'visible'; arguments[0].style.height = '1px';"
            + " arguments[0].style.width = '1px'; arguments[0].style.opacity = 1", fileElem.getWebElement());

        // Sending the keystrokes will ultimately submit the request.  No need to simulate the click
        fileElem.sendKeys(absolutePath);
    }

    public static validateItemInList(
        locator: Locator, regex: RegExp, errorMessage: string = "Could not find item in repeater"): wdPromise<void> {
        // look through each of the policies displayed
        return element.all(locator).filter((elm) => {
            // get the text within each element
            return elm.getText().then((txt) => {
                // return true/false based on match
                return regex.test(txt);
            });
        }).count().then((ct) => {
            // if ct is 1, then we found the policy
            expect(ct).toBeGreaterThan(0, errorMessage);
        });
    }

    // Return a count of items in a list
    public static getNumberOfItemsInList(locator: Locator): number {
        let count = 0;
        element.all(locator).count().then((ct) => {
            count = ct;
        });
        return count;
    }

    public static clickItemInList(locator: Locator, regex: RegExp): wdPromise<void> {
       // look through each of the policies displayed
        return element.all(locator).filter((elm) => {
            // get the text within each element
            return elm.getText().then((txt) => {
                // return true/false based on match
                return regex.test(txt);
            });
        }).first().click(); // click the first one returned
    }

    // Return actual element to then act on
    public static getItemInList(locator: Locator, regex: RegExp, index: number = 0): ElementFinder {
        // look through each of the policies displayed
        return element.all(locator).filter((elm) => {
             // get the text within each element
             return elm.getText().then((txt) => {
                 // return true/false based on match
                 return regex.test(txt);
             });
         }).get(index); // return the desired item
     }

     // Return the value by attribute if getText() isn't returning
     public static getTextByAttribute(css: string, attribute: string): wdPromise<string> {
        return element.all(by.css(css)).first().getAttribute(attribute);
     }

     // Select option from a dropdown using the label of the object you want
     public static selectFromDropdown(label: string): wdPromise<void> {
        return Utils.clickFirstVisible("option[label='" + label + "']");
     }

     // this function is used to build safe regex objects from an array of strings
     public static buildRegex(values: string[]): RegExp {
        const pattern: string[] = [];

        // start the regex pattern
        pattern.push("(");

        // loop through each value provided, adding it to the array
        values.forEach((value) => {
            // escape all the reserved characters
            const safeValue: string = value.toString().replace(/\$|\/|\+/g, (match) => "\\" + match);

            // add to array, with a trailing space
            pattern.push(safeValue);
            pattern.push("\\s");
        });

        // remove the last space added, we don't need it
        pattern.pop();

        // close the regex pattern
        pattern.push(")");

        // convert array to string
        const regexString = pattern.join("");

        // return new regex
        return new RegExp(regexString);
     }

     public static typeIfPresent(locator: Locator, text: string): wdPromise<void> {
        return element(locator).isPresent().then((go: boolean) => {
            if (go) {
                return element(locator).sendKeys(text);
            }
        });
     }
}
