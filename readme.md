#Automation Testing Template

This automation suite provides a template for writing end-to-end automation tests against an Angular website. It was created using Protractor, Jasmine, Typescript, and Gherkin. It contains a basic reporting solution, as well as a library of helpful pre-built functions to aid in testing complex scencarios.

###Installation Instructions:

####Setup Developer Tools (if neccessary):
1. Install [Git](https://git-scm.com/download/win)
2. Install [Java](http://www.oracle.com/technetwork/java/javase/downloads/jdk9-downloads-3848520.html)
_be sure to set all env variables, see info [here](http://1webdev:8090/display/AT/Java+Env+Variables)_
3. Install [Node](http://blog.teamtreehouse.com/install-node-js-npm-windows)

####Setup Global Node Tools:
1. Run `npm install -g jasmine`
2. Run `npm install -g protractor`
3. Run `npm install -g typescript`
4. Run `npm install -g tslint`

####Install Local Tools
1. Run `npm install`
2. Run `webdriver-manager update --ie`

####Test Run
1. Run `npm run tsc` to compile the typescript
2. Run `protractor tmp/conf.js  --suite main` to run a sample test suite

_tip: you can combine commands like so `rm -r tmp && npm run tsc && protractor tmp/conf.js  --suite main` to run them in sequence_