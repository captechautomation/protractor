import { browser, by, element } from "protractor";

describe("Given I am a user looking to learn more about Angular", () => {

    it("When I navigate to the Angular home page", () => {
        browser.get("https://angularjs.org/");
    });

    it("Then I am greeted with a blurb on Angular", () => {
        expect(element(by.css("div[class*='about-container']")).getText()).toContain("Why AngularJS?");
    });
});
